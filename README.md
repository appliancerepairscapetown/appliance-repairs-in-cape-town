We specialise in appliance repairs in Cape Town and our technicians have already successfully repaired appliances for thousands of households.
If you are stuck with an important household appliance that isn’t operating as it should or has broken down completely, don’t worry – we can carry out repairs that could save you against the cost of replacement. Appliances such as fridges, washing machines, tumble dryers, aircon, microwave, cookers and geysers are such an important part of everyday life, a breakdown can have a huge impact on your ability to keep things running smoothly.

If your appliance has developed a fault we will do all we can to fix it for you, fast! All our repairs are guaranteed and you can call us or book a call-out online. [Camtar Appliances](https://camtarappliances.com)
